#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import csv
import numpy as np

def readCSV():
    agents = dict()
    
    with open('spliddit/agents.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            #print(f'\t{row["name"]} works in the {row["department"]} department, and was born in {row["birthday month"]}.')
            if row["instance_id"] in agents:
                agents[row["instance_id"]].append(row["id"])
            else:
                agents[row["instance_id"]]=[row["id"]]
            line_count += 1
        #print "processed",line_count,"lines"
        
    resources = dict()
        
    with open('spliddit/resources.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            #print(f'\t{row["name"]} works in the {row["department"]} department, and was born in {row["birthday month"]}.')
            if row["instance_id"] in resources:
                resources[row["instance_id"]].append(row["id"])
            else:
                resources[row["instance_id"]]=[row["id"]]
            line_count += 1
        #print "processed",line_count,"lines"
        
    # key : instance_id, value : u
    instances = dict()
    
    for instId in agents:
        instances[instId]=np.zeros((len(agents[instId]),len(resources[instId])))
        
    with open('spliddit/valuations.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        inst_curr=-1
        ag_curr = 0
        obj_curr = 0
        count_curr = 0
        for row in csv_reader:
            if inst_curr!=row["instance_id"]: #on est sur une nouvelle instance
                inst_curr=row["instance_id"]
                ag_curr=0
                obj_curr=0
                count_curr=0
            else:
                count_curr+=1
                ag_curr=count_curr//(len(instances[inst_curr][0]))
                obj_curr=count_curr%(len(instances[inst_curr][0]))
            #print count_curr,ag_curr,obj_curr,len(instances[inst_curr]),len(instances[inst_curr][0]),inst_curr
            instances[inst_curr][ag_curr,obj_curr]=row["value"]
            #print(f'\t{row["name"]} works in the {row["department"]} department, and was born in {row["birthday month"]}.')
            if row["instance_id"] in resources:
                resources[row["instance_id"]].append(row["id"])
            else:
                resources[row["instance_id"]]=[]
            line_count += 1
        #print "processed",line_count,"lines"
    return instances
