#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import randint as r

import numpy as np
import math
import time
from gurobipy import *
from heapq import *

import networkx as nx
from networkx.algorithms import bipartite
from scipy import sparse

class System:
    
    def __init__(self,nbObj,nbAg,U,maxK=999):
        
        # PARAMETRES DU PROBLEME D'ALLOCATION MULTIAGENTS DE RESSOURCES
        
        self.n = nbObj
        self.m = nbAg
        self.u=U
        
        self.maxK=maxK
        
        self.maxFlow()
            
        # CREATION DU PL
        self.model = Model()
        self.model.setParam( 'OutputFlag', False )
        
        #self.model.setParam( 'Threads', 1 )
        self.setVariables()
        self.setConstraints()
        self.setObjective()
        #self.showRes()
        
        
    
    def maxFlow(self):
        matrix = np.zeros((self.m,self.n))
        
        for i in range(len(self.u)):
            for j in range(len(self.u[0])):
                if self.u[i][j]>0:
                    matrix[i][j]=1
        
        B = bipartite.from_biadjacency_matrix(sparse.csr_matrix(matrix))
        
        matching=nx.max_weight_matching(B)
        
        # each agent cannot get a positive valuation so we remove the agents thatdo not appear in the matching
        # we have to define the new isntance we will deal with 
        if len(matching)!=self.n: 
            self.instanceChange=True
            newn=len(matching)
            positiveAgents=[min(j) for j in matching]
            newU=np.zeros((newn,self.n))
            ind=0
            for i in range(len(self.u)):
                if i in positiveAgents:
                    for j in range(len(self.u[0])):
                        newU[ind][j] = self.u[i][j]
                    ind+=1
                    
            self.m=newn
            self.u=newU
            self.posAgents = positiveAgents
            
        # there exists a perfect matching meaning there is an allocation for which every agent has a positive utility
        else:
            self.instanceChange=False
        
    def showRes(self):
        self.model.optimize()
        logUtil = self.model.getAttr('x', self.w_var)
        affect = self.model.getAttr('x', self.z_var)
        
        print(logUtil)
        print(affect)
        
    
    def minEmptyNot0(self,l):
        """
        Redefinit min : renvoie min(l) si l est non vide, renvoie 0 sinon 
        """
        a=l.copy()
        while 0 in a:
            a.remove(0)
        if len(a)==0:
            return 0
        else:
            return min(a)
    
    def showResComplete(self):
        self.model.optimize()
        affect = self.model.getAttr('x', self.z_var)
        
        """
        blbl=[[4, 6, 10], [3, 7, 8], [1, 2], [5, 9]]
        affect=np.zeros((self.m,self.n))
        for i in range(len(blbl)):
            for val in blbl[i]:
                affect[i][val-1]=1
        """
        
        utilAg=[sum([affect[i,j]*self.u[i][j] for j in range(self.n)]) for i in range(self.m)]    #utilité de chaque agent
        itemsAg=[[j+1 for j in range(self.n) if affect[i,j]>=0.5] for i in range(self.m)]         #objets alloués à chaque agent                                                                              #agent le plus envié par chaque agent
        utilAgAg=[[sum([affect[k,j]*self.u[i][j] for j in range(self.n)]) for k in range(self.m)] for i in range(self.m)] #utilAgAg[i][j] représente l'utilité du bundle de j selon les préférences de i                                                                   #utilité de chaque agent selon chaque agent
        utilAgAgEF=[[utilAgAg[i][k]-utilAgAg[i][i] for k in range(self.m)]for i in range(self.m)]
        envyEFAg=[max(utilAgAgEF[i]+[0]) for i in range(self.m)]
        utilAgAgEF1=[[utilAgAg[i][k]-utilAgAg[i][i]-(max([self.u[i][h-1] for h in itemsAg[k]]+[0])) for k in range(self.m)]for i in range(self.m)]
        envyEF1Ag=[max(utilAgAgEF1[i]+[0]) for i in range(self.m)]
        utilAgAgEFX=[[utilAgAg[i][k]-utilAgAg[i][i]-(self.minEmpty([self.u[i][h-1] for h in itemsAg[k]])) for k in range(self.m)]for i in range(self.m)]
        envyEFXAg=[max(utilAgAgEFX[i]+[0]) for i in range(self.m)]
        utilAgAgEFX0=[[utilAgAg[i][k]-utilAgAg[i][i]-(self.minEmptyNot0([self.u[i][h-1] for h in itemsAg[k]])) for k in range(self.m)]for i in range(self.m)]
        envyEFXAgnot0=[max(utilAgAgEFX0[i]+[0]) for i in range(self.m)]
        
        # return [EF,EF1,EFX,utilTotal,valOWA]
        ef=0
        ef1=0
        efx=0
        efxnot0=0
        if envyEFAg==[0 for _ in range(self.m)]:
            ef=1
            ef1=1
            efx=1
            efxnot0=1
        elif envyEFXAg==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=1
            efxnot0=1
        elif envyEFXAgnot0==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=0
            efxnot0=1
        elif envyEF1Ag==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=0
            efxnot0=0
        #print affect
        #print self.u
        #print envyEFXAg
        if ef!=1:
            result = np.zeros((self.m,self.n))
            result-=1
            for i in range(self.m):
                for j in range(self.n):
                    if affect[i,j]>0.5:
                        result[i,j] = self.u[i,j] 
            #print(result)
            #print(self.u)
            #print(envyEF1Ag)
            
            #raise ValueError
        return [ef,efxnot0,efx,ef1,self.owa(envyEFAg),sum(utilAg),np.product(utilAg),utilAg]
        
        
    def setObjective(self):
        sumObjective = LinExpr()
        for i in range(self.m): # pour tout agent
            sumObjective.addTerms(1, self.w_var[i])
                
        self.model.setObjective(sumObjective, GRB.MAXIMIZE)
        self.model.update()
                
    def pareto2(self,l):
        """
        Applique un filtre de Pareto
        """
        remove=[]
        
        for ind1 in range(len(l)):
            for ind2 in range(len(l)):
                if ind1!=ind2:
                    dom=True
                    for i in range(len(l[ind1])):
                        dom = (l[ind1][i]>=l[ind2][i]) and dom
                    if dom:
                        #print l[ind1],"domine",l[ind2]
                        remove.append(ind2)
        """
        if len(remove)>0:
            print len(remove),"\n\n",remove,"\n\n",l
            raise ValueError
        """
            
        res=[]    
        for i in range(len(l)):
            if i not in remove:
                res.append(l[i])
                
        return res
                        
    
    def setVariables(self):
        
        # declaration des variables zij=0 si on l'agent i ne prend pas l'objet j et zij=1 si l'agent i le prend
        self.z_var = {}
        for i in range(self.m): # pour tout agent i
            for j in range(self.n): # pour tout objet j
                self.z_var[i,j] = self.model.addVar(vtype=GRB.BINARY,name='z_'+str(i)+'_'+str(j))
            
          
        
        # declaration des variables ei qui définissent le log de l'utilité de l'agent i
        self.w_var = {}
        for i in range(self.m): # pour tout agent i
            self.w_var[i] = self.model.addVar(lb=0,vtype=GRB.CONTINUOUS,name='w_'+str(i))
            
                
        self.model.update()
        
    def setConstraints(self):
                
        
        # contrainte sur le log des utilités des agents
        for i in range(self.m): # pour tout agent
            obji = LinExpr()
            for j in range(self.n): # pour tout objet
                obji.addTerms(self.u[i][j], self.z_var[i,j])
            for k in range(1,self.maxK):
                self.model.addConstr(self.w_var[i]-(math.log(k+1)-math.log(k))*obji, GRB.LESS_EQUAL, math.log(k)-k*(math.log(k+1)-math.log(k)))
                
            
        
        # contrainte un objet appartient à un seul agent
        for j in range(self.n): # pour tout objet
            obji = LinExpr()
            for i in range(self.m): # pour tout agent
                obji.addTerms(1, self.z_var[i,j])
            self.model.addConstr(obji, GRB.EQUAL, 1)
        
        self.model.update()
    
    def minEmpty(self,l):
        """
        Redefinit min : renvoie min(l) si l est non vide, renvoie 0 sinon 
        """
        if len(l)==0:
            return 0
        else:
            return min(l)
        
    def owa(self,s):
        """
        renvoie la valeur de l owa associée au vecteur
        """
        nbAgs=self.m
        owa_weights = np.zeros(nbAgs)
        owa_weights[0]=1
        
        owa_weights2 = np.zeros(nbAgs)
        for indOw in range(nbAgs):
            owa_weights2[indOw]=1/(1.0*(2**(indOw+1)))
            
        owa_weights3 = np.random.rand(nbAgs)
        
        owa_weights4 = np.ones((nbAgs))
        
        owaw=owa_weights2
        sumowaw = owaw.sum()
        for i in range(len(owaw)):
            owaw[i]=owaw[i]/(sumowaw)
        owaw=np.sort(owaw)[::-1] #poids decroissants
        
        res=0
        s.sort()
        s.reverse() # min OWA so decreasing vectors
        for i in range(len(s)):
            res+=s[i]*owaw[i]
            
        return res
    
           
        
        
        
        
        
