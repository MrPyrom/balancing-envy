#!/usr/bin/python3
# -*- coding: utf-8 -*-

import randint as r

import csvReader as csv

import systemOwaEnv as sysGen

import systemIsPareto as sysIsPareto

import systemNash as sysNash

#import generation
#import solving
#import fairdiv
import sys

import random
import numpy as np
import time
from heapq import *
import os

def addVal(i,res,t,boolean=True):
    ef[i].append(res[0])
    efx0[i].append(res[2])
    efx[i].append(res[1])
    ef1[i].append(res[3])
    resSol = [round(x) for x in res[-1]]
    if boolean:
        sysPar = sysIsPareto.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=np.ones(nbAgs),sol=resSol,meth=1)
        boolPareto = sysPar.isPareto()
        Pareto[i].append(boolPareto)
    else:
        Pareto[i].append(1)
    timee[i].append(t)
    
if __name__=="__main__": 
    #np.random.seed(12222222)
    
    # recuperation des instances de spliddit
    instances = csv.readCSV()
    
    
    stats = dict()
    
    for key in instances:
        key_gen = str(len(instances[key]))+","+str(len(instances[key][0]))
        if key_gen in stats:
            stats[key_gen]+=1
        else:
            stats[key_gen]=1
            
    #print stats
    
    list_keys = stats.keys()
    
    #print instances['18']
    
    
    # ["minOWA 1,0,0","minOWA 1/2,1/4...","minOWA 1,1,1,1",maxNash]
    ef = [[] for _ in range(4)]
    ef1 = [[] for _ in range(4)]
    efx = [[] for _ in range(4)]
    efx0 = [[] for _ in range(4)]
    Pareto = [[] for _ in range(4)]
    timee = [[] for _ in range(4)]
    uAGarder = []
    nbIterations = 1
        
    for key in instances:
        #print "\n"
        #u = np.zeros((nbAgs,nbObjs))
        u = instances[key]
        
        nbAgs = len(u)
        nbObjs = len(u[0])
        
        if nbAgs<=nbObjs: 
            
            
            
            u = u.astype(int, copy=False)
            
            start_time = time.time()
                        
            # on utilise les poids (1,0,....,0) pour revenir a MinMax
            owa_weights = np.zeros(nbAgs)
            owa_weights[0]=1
            
            owa_weights2 = np.zeros(nbAgs)
            for indOw in range(nbAgs):
                owa_weights2[indOw]=1/(1.0*(2**(indOw+1)))
            
            owa_weights3 = np.ones((nbAgs))
            
            owa_weightsRand = np.random.rand(nbAgs)
            
            
            sys1 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights,forced=False)
            deb = time.time()
            #sys.showRes()
            res1 = sys1.showResveryVerbose()
            #print("INSTANCE NUM",nbIte+1)
            fin=time.time()-deb
            
            addVal(0,res1,fin)
            
            sys2 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights2,forced=False)
            deb = time.time()
            #sys.showRes()
            fin=time.time()-deb
            res2 = sys2.showResveryVerbose()
            #print("INSTANCE NUM",nbIte+1)
            
            addVal(1,res2,fin)
        
            sys3 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights3,forced=False)
            deb = time.time()
            #sys.showRes()
            fin=time.time()-deb
            res3 = sys3.showResveryVerbose()
            #print("INSTANCE NUM",nbIte+1)
            
            if res1[3]+res2[3]+res3[3]==0:
                print(u)
                #raise ValueError
                uAGarder.append(u)
            
            addVal(2,res3,fin)
                    
            sys4 = sysNash.System(nbObj=nbObjs,nbAg=nbAgs,U=u,maxK=1000)
            deb = time.time()
            #sys.showRes()
            fin=time.time()-deb
            res4 = sys4.showResComplete()        
            
            
            addVal(3,res4,fin,not(sys4.instanceChange))
            
    print(ef)
    print(ef1)
    print(efx)
    print(efx0)
    print(Pareto)
    print(timee)
    print(uAGarder)
    sys.exit(0)

if __name__=="__main2__": 
    
    instancesToTest=[np.array([[111,  96, 117, 184, 246, 246], [168, 167, 173, 167, 167, 158],       [ 16,  18, 912,  15,  18,  21],       [ 55, 292, 227, 130, 296,   0],      [ 19,  23, 906,  10,  18,  24]]), np.array([[ 750,   50,   50,   50,   50,   50],       [1000,    0,    0,    0,    0,    0],       [ 500,  100,  100,  100,  100,  100]]), np.array([[494, 247, 155, 104,   0],       [487, 268, 160,  85,   0],       [691, 113, 112,  84,   0],       [948,  52,   0,   0,   0],       [811, 146,  43,   0,   0]]), np.array([[   0,    0,    0,    0, 1000],       [   4,    0,    0,    0,  996],       [   0,    0,    0,    0, 1000],       [ 166,  195,    0,    0,  639],       [  73,   72,   63,    0,  792]])]
    instancesToTest=[np.array([[20, 2, 2, 2, 4],[20, 2, 2, 2, 4],[13, 1, 1, 1, 14],[0, 0, 0, 0, 30]])]
    for u in [instancesToTest[0]]:
        print(u)
        nbAgs = len(u)
        nbObjs = len(u[0])
        
        # on utilise les poids (1,0,....,0) pour revenir a MinMax
        owa_weights = np.zeros(nbAgs)
        owa_weights[0]=1
        
        owa_weights2 = np.zeros(nbAgs)
        for indOw in range(nbAgs):
            owa_weights2[indOw]=1/(1.0*(2**(indOw+1)))
        
        owa_weights3 = np.ones((nbAgs))
        
        owa_weightsRand = np.random.rand(nbAgs)
        
        sys1 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights,forced=False)
        deb = time.time()
        res1 = sys1.showResveryVerbose()
        
        
        sys2 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights2,forced=False)
        deb = time.time()
        #sys.showRes()
        fin=time.time()-deb
        res2 = sys2.showResveryVerbose()
        #print("INSTANCE NUM",nbIte+1)
        
        sys3 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights3,forced=False)
        deb = time.time()
        #sys.showRes()
        fin=time.time()-deb
        res3 = sys3.showResveryVerbose()
        #print("INSTANCE NUM",nbIte+1)
        
        if res1[3]+res2[3]+res3[3]!=0:
            raise ValueError
            print(u)
        
        """
        Nash
        sys5 = sysNash.System(nbObj=nbObjs,nbAg=nbAgs,U=u,maxK=1000)
        res5 = sys5.showResComplete()   
        
        print("Nash :",res5)
        raise ValueError
        """
        
        sys4 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weightsRand,forced=False)
        res4 = sys4.showResveryVerbose()
        #print("INSTANCE NUM",nbIte+1)
        iterr=0
        iterrLimit=10000
        while res4[3]==0 and iterr<iterrLimit:
            iterr+=1
            if iterr%10000==0:
                print(iterr)
            owa_weightsRand = np.random.rand(nbAgs)
            sys4 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weightsRand,forced=False)
            res4 = sys4.showResveryVerbose()
            print(res4)
        if iterr==iterrLimit:
            print("rien trouvé")
        else:
            print("permet EF1 :",np.sort(owa_weightsRand)[::-1],"\n\n\nNouvelle instance\n\n\n")

def owa(owaw,s):
    """
    renvoie la valeur de l owa associée a la solution s (vecteur des valuations de chaque agent)
    """
    res=0
    s.sort()
    s=s[::-1]
    #print("le fameux vecteur",s)
    for i in range(len(s)):
        res+=s[i]*owaw[i]
    
    #print("le résultat",res)
            
    return res    

if __name__=="__main2__": 
    
    while True:
        owa_weightsRand = np.random.rand(3)
        poids=np.sort(owa_weightsRand)[::-1]
        poids=np.sort([2/3,0,1/3])[::-1]
        vect1=[500,500,0]
        vect2=[750,0,0]
        vect3=[650,200,0]
        if owa(poids,vect3)<=owa(poids,vect2) and owa(poids,vect3)<=owa(poids,vect1):
            print(poids)
            raise ValueError
        

