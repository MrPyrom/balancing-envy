#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import randint as r


import numpy as np
import math
import time
from gurobipy import *
from heapq import *

class System:
    
    def __init__(self,nbObj,nbAg,U,owaw,forced=False):
        
        # PARAMETRES DU PROBLEME D'ALLOCATION MULTIAGENTS DE RESSOURCES
        
        self.n = nbObj
        self.m = nbAg
        self.u=U
        
        
        self.owaw = owaw
        self.owawp = np.zeros(len(self.owaw))
               
        
        # Forcer l instance à résoudre
        if forced:
            u1=[3,1]
            u2=[1,3]
            for j in range(len(self.u[0])):
                self.u[0][j]=u1[j]
                self.u[1][j]=u2[j]
            
        sumowaw = self.owaw.sum()
        for i in range(len(self.owaw)):
            self.owaw[i]=self.owaw[i]/(sumowaw)
        self.owaw=np.sort(self.owaw)[::-1] #poids decroissants
        
        #print("poids",self.owaw)
        #time.sleep(10)
        
        self.owawp[len(self.owawp)-1] = self.owaw[len(self.owaw)-1]
        for i in range(len(self.owaw)-1):
            self.owawp[i]=self.owaw[i]-self.owaw[i+1]
            
        # CREATION DU PL
        self.model = Model()
        self.model.setParam( 'OutputFlag', False )
        
        #self.model.setParam( 'Threads', 1 )
        self.setVariables()
        self.setConstraints()
        self.setObjective()
        #self.showRes()
        
        
        
        
        
    def showRes(self):
        self.model.optimize()
        envies = self.model.getAttr('x', self.e_var)
        affect = self.model.getAttr('x', self.z_var)
        
        #print(envies)
        #print(affect)
        
    def showResveryVerbose(self):
        self.model.optimize()
        affect = self.model.getAttr('x', self.z_var)
                
        utilAg=[sum([affect[i,j]*self.u[i][j] for j in range(self.n)]) for i in range(self.m)]    #utilité de chaque agent
        itemsAg=[[j+1 for j in range(self.n) if affect[i,j]>=0.5] for i in range(self.m)]         #objets alloués à chaque agent                                                                              #agent le plus envié par chaque agent
        utilAgAg=[[sum([affect[k,j]*self.u[i][j] for j in range(self.n)]) for k in range(self.m)] for i in range(self.m)] #utilAgAg[i][j] représente l'utilité du bundle de j selon les préférences de i                                                                   #utilité de chaque agent selon chaque agent
        utilAgAgEF=[[utilAgAg[i][k]-utilAgAg[i][i] for k in range(self.m)]for i in range(self.m)]
        envyEFAg=[max(utilAgAgEF[i]+[0]) for i in range(self.m)]
        utilAgAgEF1=[[utilAgAg[i][k]-utilAgAg[i][i]-(max([self.u[i][h-1] for h in itemsAg[k]]+[0])) for k in range(self.m)]for i in range(self.m)]
        envyEF1Ag=[max(utilAgAgEF1[i]+[0]) for i in range(self.m)]
        utilAgAgEFX=[[utilAgAg[i][k]-utilAgAg[i][i]-(self.minEmpty([self.u[i][h-1] for h in itemsAg[k]])) for k in range(self.m)]for i in range(self.m)]
        envyEFXAg=[max(utilAgAgEFX[i]+[0]) for i in range(self.m)]
        utilAgAgEFX0=[[utilAgAg[i][k]-utilAgAg[i][i]-(self.minEmptyNot0([self.u[i][h-1] for h in itemsAg[k]])) for k in range(self.m)]for i in range(self.m)]
        envyEFXAgnot0=[max(utilAgAgEFX0[i]+[0]) for i in range(self.m)]
        
        # return [EF,EF1,EFX,utilTotal,valOWA]
        ef=0
        ef1=0
        efx=0
        efxnot0=0
        if envyEFAg==[0 for _ in range(self.m)]:
            ef=1
            ef1=1
            efx=1
            efxnot0=1
        elif envyEFXAg==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=1
            efxnot0=1
        elif envyEFXAgnot0==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=0
            efxnot0=1
        elif envyEF1Ag==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=0
            efxnot0=0
        #print(envyEF1Ag)
        if ef1==1:
            #print(self.owaw)
            nothing=1
        #print affect
        #print self.u
        #print envyEFXAg
        result = np.zeros((self.m,self.n))
        result-=1
        for i in range(self.m):
            for j in range(self.n):
                if affect[i,j]>0.5:
                    result[i,j] = self.u[i,j] 
            #raise ValueError
        #print("Affectation : ",itemsAg)
        #print("Utilité de chaque agent",utilAg)
        return [ef,efxnot0,efx,ef1,self.owa(envyEFAg),sum(utilAg),np.product(utilAg),utilAg]
        
        
    def setObjective(self):
        # fonction objectif owa full info
        sumObjective1 = LinExpr()
        sumObjective2 = LinExpr()
        for k in range(self.m):
            sumObjective1.addTerms(self.owawp[k]*(k+1), self.r_var[k])
            for i in range(self.m):
                sumObjective2.addTerms(self.owawp[k], self.b_var[i,k])
                
        self.model.setObjective(sumObjective1+sumObjective2, GRB.MINIMIZE)
        self.model.update()
                
    def owa(self,s):
        """
        renvoie la valeur de l owa associée a la solution s (vecteur des valuations de chaque agent)
        """
        res=0
        s.sort()
        s=s[::-1]
        #print("le fameux vecteur",s)
        for i in range(len(s)):
            res+=s[i]*self.owaw[i]
        
        #print("le résultat",res)
                
        return res
    
    def setVariables(self):
        
        # declaration des variables zij=0 si on l'agent i ne prend pas l'objet j et zij=1 si l'agent i le prend
        self.z_var = {}
        for i in range(self.m): # pour tout agent i
            for j in range(self.n): # pour tout objet j
                self.z_var[i,j] = self.model.addVar(vtype=GRB.BINARY,name='z_'+str(i)+'_'+str(j))
            
        # decalration des variables bik (variables du dual de la k-ieme composante de Lorenz)
        self.b_var = {}
        for i in range(self.m):
            for k in range(self.m):
                self.b_var[i,k] = self.model.addVar(lb=0,vtype=GRB.CONTINUOUS,name='b_'+str(i)+'_'+str(k))
          
        
        # declaration des variables ei qui définissent l'envie de l'agent i
        self.e_var = {}
        for i in range(self.m): # pour tout agent i
            self.e_var[i] = self.model.addVar(lb=0,vtype=GRB.CONTINUOUS,name='e_'+str(i))
            
        # decalration des variables rk (variables du dual de la k-ieme composante de Lorenz)
        self.r_var = {}
        for k in range(self.m): #agent
            self.r_var[k] = self.model.addVar(vtype=GRB.CONTINUOUS,name='r_'+str(i)+'_'+str(j))
                
        self.model.update()
        
    def setConstraints(self):
        
        # contrainte définissant l'envie de chaque agent comme max
        for i in range(self.m): # pour tout agent
            for other in range(self.m): # pour tout autre agent
                if i!=other:
                    utili = LinExpr()
                    utilothers = LinExpr()
                    for j in range(self.n): # pour tout objet
                        utili.addTerms(self.u[i][j], self.z_var[i,j])
                        utilothers.addTerms(self.u[i][j], self.z_var[other,j])
                
                    self.model.addConstr(self.e_var[i], GRB.GREATER_EQUAL,utilothers-utili)
        
        
        # contrainte un objet appartient à un seul agent
        for j in range(self.n): # pour tout objet
            obji = LinExpr()
            for i in range(self.m): # pour tout agent
                obji.addTerms(1, self.z_var[i,j])
            self.model.addConstr(obji, GRB.EQUAL, 1)
        
        
        # contrainte tous les objets sont alloués
        alloc = LinExpr()
        for j in range(self.n): # pour tout objet
            for i in range(self.m): # pour tout agent
                alloc.addTerms(1, self.z_var[i,j])
        self.model.addConstr(alloc, GRB.EQUAL, self.n)
        
        
        
        # contrainte (1) linéarisation
        for i in range(self.m): # pour tout agent
            for k in range(self.m):
                self.model.addConstr(self.r_var[k]+self.b_var[i,k]-self.e_var[i], GRB.GREATER_EQUAL, 0)
             
        
        self.model.update()
    

    def minEmpty(self,l):
        """
        Redefinit min : renvoie min(l) si l est non vide, renvoie 0 sinon 
        """
        if len(l)==0:
            return 0
        else:
            return min(l)
    
    def minEmptyNot0(self,l):
        """
        Redefinit min : renvoie min(l) si l est non vide, renvoie 0 sinon 
        """
        a=l.copy()
        while 0 in a:
            a.remove(0)
        if len(a)==0:
            return 0
        else:
            return min(a)

