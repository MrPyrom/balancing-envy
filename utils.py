#!/usr/bin/python3
"""io utils for voting profiles."""

import math
import os
import sys

#TERM_SIZE = int(os.popen('stty size', 'r').read().split()[1])


def read_preflib_soc(file):
    """Reads a .soc preflib file (complete strict order).

    Arguments:
    - file: the file to read (not the filename)."""
    nb_cand = int(file.readline())
    for _ in range(nb_cand):
        file.readline()
    _, nb_voters, nb_lines = [int(x) for x in file.readline().split(',')]
    votes = []
    counter = 0
    for _ in range(nb_lines):
        line_split = file.readline().split(',')
        nb = int(line_split[0])
        votes += [[(int(v) - 1) for v in line_split[1:]] for _ in range(nb)]
        counter += nb
    assert counter == nb_voters, "Number of voters does not match! {} != {}".format(nb_voters, nb)
    return votes


def print_progress(prob_number, total_number):
    step = 10 ** int(math.log10(total_number / 1000))

    if total_number < 10000 or prob_number % step == 0:
        ratio = (prob_number * (TERM_SIZE - 20)) // total_number
        print('\r|' + ('=' * ratio) + '>' + (' ' * (TERM_SIZE - 20 - ratio))
              + '| ' + abbrv(prob_number) + '/' + abbrv(total_number), end='')
        sys.stdout.flush()


def abbrv(value):
    if value < 1:
        return str(value)
    order = math.log10(value)
    if order >= 9:
        return ("%.2fG" % (value / 1000000000))
    if order >= 6:
        return ("%.2fM" % (value / 1000000))
    if order >= 3:
        return ("%.2fK" % (value / 1000))
    return str(value)
