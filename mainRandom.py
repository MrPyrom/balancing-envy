#!/usr/bin/python3
# -*- coding: utf-8 -*-

import randint as r

import csvReader as csv

import systemOwaEnv as sysGen

import systemIsPareto as sysIsPareto

import systemNash as sysNash

#import generation
#import solving
#import fairdiv
import sys

import random
import numpy as np
import time
from heapq import *
import os

def addVal(i,res,t,boolean=True):
    ef[i].append(res[0])
    efx0[i].append(res[2])
    efx[i].append(res[1])
    ef1[i].append(res[3])
    resSol = [round(x) for x in res[-1]]
    if boolean:
        sysPar = sysIsPareto.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=np.ones(nbAgs),sol=resSol,meth=1)
        boolPareto = sysPar.isPareto()
        Pareto[i].append(boolPareto)
    else:
        Pareto[i].append(1)
    timee[i]+=t/100
    
if __name__=="__main__": 
    #np.random.seed(12222222)
    
    # recuperation des instances de spliddit
    instances = csv.readCSV()
    
    
    stats = dict()
    
    for key in instances:
        key_gen = str(len(instances[key]))+","+str(len(instances[key][0]))
        if key_gen in stats:
            stats[key_gen]+=1
        else:
            stats[key_gen]=1
            
    #print stats
    
    list_keys = stats.keys()
    
    #print instances['18']
    
    agObjList = [[2,3],[3,4],[4,5],[5,7],[6,8],[7,9],[8,10],[9,11],[10,12]]
    
    forced=False                    # True si on veut rentrer les utilités à la main
    samePref=False		    # True si on veut que les agents aient tous les mêmes préférences
    
    tabRes = {i:[0 for j in range(17)] for i in list_keys}
    tabTime = {i:[0 for j in range(17)] for i in list_keys}
    
    
    nbIterations = 100
    
    # de 0 à 8
    #indagObj = 3
    
    #agObjList = [agObjList0[indagObj]]
    for indagObj in range(len(agObjList)):
        
        # ["minOWA 1,0,0","minOWA 1/2,1/4...","minOWA 1,1,1,1",maxNash]
        ef = [[] for _ in range(4)]
        ef1 = [[] for _ in range(4)]
        efx = [[] for _ in range(4)]
        efx0 = [[] for _ in range(4)]
        Pareto = [[] for _ in range(4)]
        timee = [0 for _ in range(4)]
        
        agObj = agObjList[indagObj]
        nbAgs = agObj[0]
        nbObjs = agObj[1]

        normalU = 5*nbObjs            # la valeur a laquelle somme l'utilité de tout agent
        u = np.zeros((nbAgs,nbObjs))
        
        for _ in range(nbIterations):
            for ag in range(nbAgs):
                u[ag] = r.RandIntVec(nbObjs, normalU, Distribution=r.RandFloats(nbObjs))
                        
            max_weight = 100
            
            #print(u,"\n")
            u = u.astype(int, copy=False)
            #print(u,type(u[0,0]))
            start_time = time.time()
            
            # on utilise les poids (1,0,....,0) pour revenir a MinMax
            owa_weights = np.zeros(nbAgs)
            owa_weights[0]=1
            
            owa_weights2 = np.zeros(nbAgs)
            for indOw in range(nbAgs):
                owa_weights2[indOw]=1/(1.0*(2**(indOw+1)))
            
            owa_weights3 = np.ones((nbAgs))
            
            
            sys1 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights,forced=False)
            deb = time.time()
            #sys.showRes()
            res1 = sys1.showResveryVerbose()
            #print("INSTANCE NUM",nbIte+1)
            fin=time.time()-deb
            
            addVal(0,res1,fin)
            
            sys2 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights2,forced=False)
            deb = time.time()
            #sys.showRes()
            res2 = sys2.showResveryVerbose()
            #print("INSTANCE NUM",nbIte+1)
            
            fin=time.time()-deb
            
            addVal(1,res2,fin)
        
            sys3 = sysGen.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights3,forced=False)
            deb = time.time()
            #sys.showRes()
            res3 = sys3.showResveryVerbose()
            #print("INSTANCE NUM",nbIte+1)
            
            fin=time.time()-deb
            
            addVal(2,res3,fin)
                    
            sys4 = sysNash.System(nbObj=nbObjs,nbAg=nbAgs,U=u,maxK=normalU)
            deb = time.time()
            #sys.showRes()
            res4 = sys4.showResComplete()        
            
            fin=time.time()-deb
            
            addVal(3,res4,fin,not(sys4.instanceChange))
        
        print("ef[",indagObj,"]=",ef)
        print("efx0[",indagObj,"]=",efx0)
        print("efx[",indagObj,"]=",efx)
        print("ef1[",indagObj,"]=",ef1)
        print("Pareto[",indagObj,"]=",Pareto)
        print("t[",indagObj,"]=",timee,"\n\n")
    #sys.exit(0)

