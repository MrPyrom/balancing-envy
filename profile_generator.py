#!/usr/bin/python3
"""This module is dedicated to the generation of voting profiles."""

from random import shuffle, randint, random, sample
from math import factorial
from itertools import permutations, product
from functools import cmp_to_key
from utils import read_preflib_soc, print_progress
import numpy as np
from numpy.lib.format import open_memmap
#import probas

# Cultures ########################################
# La culture, c'est comme la confiture

#pylint: disable=too-few-public-methods
class Culture(object):
    """Represents a culture for generating profiles."""
    def __init__(self, nb_candidates, nb_voters):
        self.nb_candidates = nb_candidates
        self.nb_voters = nb_voters

# No restriction ##################################
# Exhaustive generation ###########################

class Exhaustive(Culture):
    """The exhaustive complete preferences."""
    def __init__(self, nb_candidates, nb_voters):
        Culture.__init__(self, nb_candidates, nb_voters)

    def profiles(self):
        """Generator of all the possible permutation profiles."""
        return product(permutations(range(self.nb_candidates)),
                       repeat=self.nb_voters)

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return factorial(self.nb_candidates) ** self.nb_voters


class FromFile(Culture):
    """Reads a set of profiles from a file."""
    def __init__(self, filename):
        fp = open_memmap(filename, mode='r')
        self.memo = np.array(fp)
        Culture.__init__(self, fp.shape[2], fp.shape[1])

    def profiles(self):
        """Generator of all the possible permutation profiles."""
        yield from self.memo

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return np.shape(self.memo)[0]


class HashableProfile():
    def __init__(self, raw_profile):
        self.raw_profile = np.array(raw_profile)
        self.n = len(raw_profile)
        self.m = len(raw_profile[0])
        self.mult_array = np.array(
            [[(self.m + 1) ** (self.m * i + j) for j in range(self.m)]
             for i in range(self.n)])

    def __eq__(self, other):
        return (self.raw_profile == other.raw_profile).all()

    def __hash__(self):
        return int(sum(sum(self.raw_profile * self.mult_array)))


class Roots(Culture):
    """All roots of profiles."""
    def __init__(self, nb_candidates, nb_voters):
        Culture.__init__(self, nb_candidates, nb_voters)
        self.memo = set()
        self.probed = 0
        self.to_probe = factorial(self.nb_candidates) ** (self.nb_voters - 1)
        self.verbose = False

    @staticmethod
    def _lexico_cmp(l1, l2):
        """Lexicographic comparison of lists of the same size."""
        for a, b in zip(l1, l2):
            if a != b:
                return -1 if a < b else 1
        return 0

    @staticmethod
    def _test_permutations(raw_profile, memo):
        for permutation in permutations(range(len(raw_profile[0]))):
            permuted_profile = [[permutation[x] for x in row]
                                for row in raw_profile]
            permuted_profile = HashableProfile(
                sorted(permuted_profile,
                       key=cmp_to_key(Roots._lexico_cmp)))

            if permuted_profile in memo:
                return False

        return True

    @staticmethod
    def _test_lex_col(raw_profile):
        for i in range(len(raw_profile) - 1):
            if Roots._lexico_cmp(raw_profile[i], raw_profile[i+1]) > 0:
                return False
        return True

    @staticmethod
    def _simple_test_signature(raw_profile):
        n = len(raw_profile)
        m = len(raw_profile[0])
        transposed_profile = np.transpose(raw_profile)[0]
        k = 0
        nb_a = 0
        current = 0
        i = 0
        while i < m:
            if transposed_profile[i] == 0:
                nb_a += 1
                if nb_a >= n - m + 1:
                    return True
            elif transposed_profile[i] == k:
                if nb_a < (n - nb_a) / (m - 1):
                    return False
                current += 1
                if current > nb_a:
                    return False
            else:
                current = 0
                k += 1
            i += 1
        if current > nb_a:
            return False
        return True

    def profiles(self):
        """Generator of all the roots. That is: one representative for each
        equivalence class of the relation stating that two profiles P1 and P2
        are equivalent iff P2 can be obtained from P1 by a permutation of
        candidates and voters."""

        for raw_profile in product(permutations(range(self.nb_candidates)),
                                   repeat=self.nb_voters - 1):
            raw_profile = np.array(raw_profile)
            if Roots._test_lex_col(raw_profile):
                raw_profile = np.array([tuple(range(self.nb_candidates))]
                                       + list(raw_profile))
                raw_profile = sorted(raw_profile,
                                     key=cmp_to_key(Roots._lexico_cmp))
                if Roots._simple_test_signature(raw_profile)\
                   and Roots._test_permutations(raw_profile, self.memo):
                    self.memo.add(HashableProfile(raw_profile))
                    yield raw_profile
            self.probed += 1
            if self.verbose:
                print_progress(self.probed, self.to_probe)
        self.exhausted = True

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        if self.probed == self.to_probe:
            return len(self.memo)
        raise Exception("Cannot give the number of profiles yet.")

    def serialize(self, filename):
        """Serialize the list of profiles as a memmap"""
        if self.probed != self.to_probe:
            raise Exception("Cannot serialize yet "
                            + "(the iterator should be exhausted).")
        fp = open_memmap(filename, dtype='int64', mode='w+',
                         shape=(len(self.memo), self.nb_voters,
                                self.nb_candidates))
        fp[:] = np.array([p.raw_profile for p in self.memo])
        del fp


# Impartial culture ###############################

class RandomSampleICM(type):
    def __str__(cls):
        return "Impartial Culture"

class RandomSampleIC(Culture, metaclass=RandomSampleICM):
    """The random sample Impartial Culture."""

    def __init__(self, nb_candidates, nb_voters, nb_samples):
        Culture.__init__(self, nb_candidates, nb_voters)
        self.nb_samples = nb_samples

    def profiles(self):
        """Generates a given number of random profiles. If random.shuffle
        is correct, the probability distribution corresponds to
        impartial culture."""
        for _ in range(self.nb_samples):
            yield [self._random_pref(self.nb_candidates) for _ in range(self.nb_voters)]

    @staticmethod
    def _random_pref(nb_candidates):
        pref = list(range(nb_candidates))
        shuffle(pref)
        return list(pref)

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return self.nb_samples

    def __repr__(self):
        return "IC"

    def __str__(self):
        return "Impartial Culture"


    
# Mallows #######################################

class RandomSampleMallowsM(type):
    def __str__(cls):
        return "Mallows"
    
class RandomSampleMallows(Culture, metaclass=RandomSampleMallowsM):
    """The random sample Mallows. This class generates voting profiles
    according to the Mallows model with the following parameters:
    - self.refs: a set of reference rankings
    - self.phis: the corresponding set of dispersion parameters
    - self.mix: the probability distribution over the possibles refs and phis.
    """

    def __init__(self, nb_candidates, nb_voters, nb_samples, phi, nref = 1):
        """Initializes a Mallows distribution with a particular
        number of reference rankings and phi's drawn iid."""
        Culture.__init__(self, nb_candidates, nb_voters)
        self.nb_samples = nb_samples
        self.mix = []
        self.phis = []
        self.refs = []
        impartial = RandomSampleIC(self.nb_candidates, 1, nref)
        for pref in impartial.profiles():
            self.refs.append(pref[0])
            self.phis.append(round(random(), 5))
            self.mix.append(randint(1, 100))
        self.phis=[phi]
        smix = sum(self.mix)
        self.mix = [float(i) / float(smix) for i in self.mix]

    def profiles(self):
        """Generates a given number of random profiles. The probability
        distribution corresponds to the Mallows distribution with
        reference rankings self.refs, dispersion parameters self.phis
        and distribution over these rankings and phis self.mix.

        For each voter, a reference ranking and dispersion is drawn
        according to probabilities self.mix.

        Then we use the generative process Repeated Insertion Model (RIM) introduced
        by Doignon et al. (2004) and described e.g. by Lu and Boutilier (2011) to generate
        the target ranking for this ref and phi."""
	#Precompute the distros for each Phi and Ref.
	#Turn each ref into an order for ease of use...
        m_insert_dists = []
        for i in range(len(self.mix)):
            m_insert_dists.append(self._compute_mallows_insertvec_dist(i))
        
        for _ in range(self.nb_samples):
            yield self._gen_mallows(m_insert_dists)

    def _gen_mallows(self, m_insert_dists):
        cum_insert_dists = []
        for cmodel in range(len(m_insert_dists)):
            cum_insert_dists.append({})
            for i in m_insert_dists[cmodel]:
                cum_insert_dists[cmodel][i] = probas.cumulate(m_insert_dists[cmodel][i])
        #Now, generate votes...
        votes = []
        for _ in range(self.nb_voters):
            #print("drawing")
            cmodel = probas.draw(list(range(len(self.mix))), self.mix)
            #print(cmodel)
            #print(refs[cmodel])
            #Generate a vote for the selected model
            insvec = [0] * self.nb_candidates
            for i in range(1, len(insvec)+1):
                #options are 1...max
                #print("Options: " + str(list(range(1, i+1))))
                #print("Dist: " + str(insertvec_dist[i]))
                #print("Drawing on model " + str(cmodel))
                #insvec[i-1] = probas.draw(list(range(1, i+1)), m_insert_dists[cmodel][i])
                insvec[i-1] = probas.draw_cumulative(list(range(1, i+1)), cum_insert_dists[cmodel][i])
            vote = []
            for i in range(len(self.refs[cmodel])):
                vote.insert(insvec[i]-1, self.refs[cmodel][i])
                #print("mallows vote: " + str(vote))
            votes.append(vote)

        #print("votes: " + str(votes))
        return votes

    # For Phi and a given number of candidates, compute the
    # insertion probability vectors.
    # Borrowed from preflib tools
    def _compute_mallows_insertvec_dist(self, index_phi):
	#Compute the Various Mallows Probability Distros
        vec_dist = {}
        for i in range(1, self.nb_candidates + 1):
            #Start with an empty distro of length i
            dist = [0] * i
            #compute the denom = phi^0 + phi^1 + ... phi^(i-1)
            denom = sum([pow(self.phis[index_phi], k) for k in range(i)])
            #Fill each element of the distro with phi^i-j / denom
            for j in range(1, i+1):
                dist[j-1] = pow(self.phis[index_phi], i - j) / denom
            #print(str(dist) + "total: " + str(sum(dist)))
            vec_dist[i] = dist
        return vec_dist


    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return self.nb_samples

    def __repr__(self):
        return "Mallows-{}".format(''.join([str(phi) for phi in self.phis]))

    def __str__(self):
        return "Mallows phi = {}".format(','.join([str(phi) for phi in self.phis]))

"""
#nbAg
n=10
#nbObj
m=10
mallows = RandomSampleMallows(m,n,5)
print(mallows.phis)
mallows.phis=[0]
print(mallows.__str__())
for i in mallows.profiles():
    print(i)
""" 
# Single-Peaked ###################################
# Exhaustive generation ###########################

class ExhaustiveSP(Culture):
    """The exhaustive Single-Peaked."""
    def __init__(self, nb_candidates, nb_voters):
        Culture.__init__(self, nb_candidates, nb_voters)

    def profiles(self):
        """Generator of all the possible single-peaked profiles.
        Corresponds to exhaustive Single-Peaked.

        The candidate axe is supposed to be from 0 to n. Preferences
        are generated according to the algorithm given by Conitzer (AAMAS 2007):
        Eliciting Single-peaked Preferences Using Comparison Queries.
        """
        return product(self._profile_gen([-1] * self.nb_candidates),
                       repeat=self.nb_voters)

    def _profile_gen(self, order):
        for first in range(self.nb_candidates):
            order[0] = first
            yield from self._profile_gen_aux(first - 1, first + 1, order, 1)

    def _profile_gen_aux(self, left, right, order, rank):
        if left < 0:
            yield order[:rank] + list(range(right, self.nb_candidates))
        else:
            if right >= self.nb_candidates:
                yield order[:rank] + list(range(left, -1, -1))
            else:
                order[rank] = left
                yield from self._profile_gen_aux(left - 1, right, order, rank + 1)
                order[rank] = right
                yield from self._profile_gen_aux(left, right + 1, order, rank + 1)

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return (2 ** (self.nb_candidates - 1)) ** self.nb_voters

# Impartial culture ###########################

class RandomSampleSPICM(type):
    def __str__(cls):
        return "Single-Peaked Impartial Culture"

class RandomSampleSPIC(Culture, metaclass=RandomSampleSPICM):
    """The random sample Single-Peaked Impartial Culture."""

    def __init__(self, nb_candidates, nb_voters, nb_samples):
        Culture.__init__(self, nb_candidates, nb_voters)
        self.nb_samples = nb_samples

    def profiles(self):
        """Generates a given number of random profiles. If random.randint
        is correct, the probability distribution corresponds to
        impartial culture for Single-Peaked preferences."""
        for _ in range(self.nb_samples):
            yield [self._profile_gen(0, self.nb_candidates - 1,
                                     [-1] * self.nb_candidates, self.nb_candidates - 1)
                   for _ in range(self.nb_voters)]

    def _profile_gen(self, left, right, order, rank):
        if left == right:
            order[rank] = left
            return order
        else:
            if randint(0, 1):
                order[rank] = left
                return self._profile_gen(left + 1, right, order, rank - 1)
            else:
                order[rank] = right
                return self._profile_gen(left, right - 1, order, rank - 1)

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return self.nb_samples

    def __repr__(self):
        return "SPIC"

    def __str__(self):
        return "Single-Peaked Impartial Culture"

# Uniform Peak ##############################

class RandomSampleSPUPM(type):
    def __str__(cls):
        return "Single-Peaked with Uniform Peak"

class RandomSampleSPUP(Culture, metaclass=RandomSampleSPUPM):
    """The random sample Single-Peaked Impartial Culture."""

    def __init__(self, nb_candidates, nb_voters, nb_samples):
        Culture.__init__(self, nb_candidates, nb_voters)
        self.nb_samples = nb_samples

    def profiles(self):
        """Generates a given number of random profiles. If random.randint
        is correct, the probability distribution corresponds to
        impartial culture for Single-Peaked preferences."""
        for _ in range(self.nb_samples):
            yield [self._profile_gen([-1] * self.nb_candidates)
                   for _ in range(self.nb_voters)]

    def _profile_gen(self, order):
        first = randint(0, self.nb_candidates - 1)
        order[0] = first
        return self._profile_gen_aux(first - 1, first + 1, order, 1)

    def _profile_gen_aux(self, left, right, order, rank):
        if left < 0:
            return order[:rank] + list(range(right, self.nb_candidates))
        else:
            if right >= self.nb_candidates:
                return order[:rank] + list(range(left, -1, -1))
            else:
                if randint(0, 1):
                    order[rank] = left
                    return self._profile_gen_aux(left - 1, right, order, rank + 1)
                else:
                    order[rank] = right
                    return self._profile_gen_aux(left, right + 1, order, rank + 1)

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return self.nb_samples

    def __repr__(self):
        return "SPUP"

    def __str__(self):
        return "Single Peaked wit Uniform Peak"


# Real-world data #################################
# Preflib sample ##################################

class RandomSamplePreflib(Culture):
    """The random Preflib SOC sample."""

    def __init__(self, soc_file, nb_voters, nb_samples):
        """Initializes a random sample Preflib generator.

        Arguments:
        - soc_file: the Preflib soc file in which the voters' preferences will be chosen.
        - nb_voters: the number of voters.
        - nb_samples: the number of samples."""
        self.base_profile = read_preflib_soc(soc_file)
        Culture.__init__(self, len(self.base_profile[0]), nb_voters)
        self.nb_samples = nb_samples

    def profiles(self):
        """Generates a given number of profiles uniformly randomly chosen
        from the base profile."""
        for _ in range(self.nb_samples):
            yield sample(self.base_profile, self.nb_voters)

    def nb_profiles(self):
        """Returns the number of profiles for this culture."""
        return self.nb_samples
