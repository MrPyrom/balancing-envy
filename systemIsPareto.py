#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import randint as r

#import pulp

#import systemNash as sysNash

import random
import numpy as np
import math
import time
import itertools
from gurobipy import *
from heapq import *

class System:
    
    def __init__(self,nbObj,nbAg,U,owaw,sol,meth=0):
        
        # PARAMETRES DU PROBLEME D'ALLOCATION MULTIAGENTS DE RESSOURCES
        
        self.n = nbObj
        self.m = nbAg
        self.u = U
        
        # la solution dont on veut verifier la Pareto Optimalité (liste taille nbAgents des utilités de chaque agent)
        self.sol = sol
        
        
        self.owaw = owaw
        self.owawp = np.zeros(len(self.owaw))
               
            
        sumowaw = self.owaw.sum()
        for i in range(len(self.owaw)):
            self.owaw[i]=self.owaw[i]/(sumowaw)
        self.owaw=np.sort(self.owaw)[::-1] #poids decroissants
        
        self.owawp[len(self.owawp)-1] = self.owaw[len(self.owaw)-1]
        for i in range(len(self.owaw)-1):
            self.owawp[i]=self.owaw[i]-self.owaw[i+1]
            
        #print(self.owaw)
        # CREATION DU PL
        self.model = Model()
        self.model.setParam( 'OutputFlag', False)
        
        #self.model.setParam( 'Threads', 1 )
        self.setVariables()
        self.setConstraints()
        self.setObjective(meth)
        #self.showRes()
        
    def isPareto(self):
        self.model.optimize()
        
        affect = self.model.getAttr('x', self.z_var)
        
        utilAg=[sum([affect[i,j]*self.u[i][j] for j in range(self.n)]) for i in range(self.m)]
        
        #print(utilAg)
        
        if sum(utilAg)==sum(self.sol):
            return 1
        else:
            return 0
            
        
    def showRes(self):
        self.model.optimize()
        affect = self.model.getAttr('x', self.z_var)
        
        utilAg=[sum([affect[i,j]*self.u[i][j] for j in range(self.n)]) for i in range(self.m)]    #utilité de chaque agent
        itemsAg=[[j+1 for j in range(self.n) if affect[i,j]>=0.5] for i in range(self.m)]         #objets alloués à chaque agent                                                                              #agent le plus envié par chaque agent
        utilAgAg=[[sum([affect[k,j]*self.u[i][j] for j in range(self.n)]) for k in range(self.m)] for i in range(self.m)] #utilAgAg[i][j] représente l'utilité du bundle de j selon les préférences de i                                                                   #utilité de chaque agent selon chaque agent
        utilAgAgEF=[[utilAgAg[i][k]-utilAgAg[i][i] for k in range(self.m)]for i in range(self.m)]
        envyEFAg=[max(utilAgAgEF[i]+[0]) for i in range(self.m)]
        utilAgAgEF1=[[utilAgAg[i][k]-utilAgAg[i][i]-(max([self.u[i][h-1] for h in itemsAg[k]]+[0])) for k in range(self.m)]for i in range(self.m)]
        envyEF1Ag=[max(utilAgAgEF1[i]+[0]) for i in range(self.m)]
        utilAgAgEFX=[[utilAgAg[i][k]-utilAgAg[i][i]-(self.minEmpty([self.u[i][h-1] for h in itemsAg[k]])) for k in range(self.m)]for i in range(self.m)]
        envyEFXAg=[max(utilAgAgEFX[i]+[0]) for i in range(self.m)]
        
        # return [EF,EF1,EFX,utilTotal,valOWA]
        ef=0
        ef1=0
        efx=0
        if envyEFAg==[0 for _ in range(self.m)]:
            ef=1
            ef1=1
            efx=1
        elif envyEFXAg==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=1
        elif envyEF1Ag==[0 for _ in range(self.m)]:
            ef=0
            ef1=1
            efx=0
        #print affect
        #print self.u
        #print envyEFXAg
        if ef1!=1:
            result = np.zeros((self.m,self.n))
            result-=1
            for i in range(self.m):
                for j in range(self.n):
                    if affect[i,j]>0.5:
                        result[i,j] = self.u[i,j] 
            print(result)
            print(self.u)
            print(envyEF1Ag)
            
            #raise ValueError
        print(self.u)
#        print(self.pvarMaxAllAgent)
        print("Affectation : ",itemsAg)
        print("Utilité de chaque agent",utilAg)
        return [ef,efx,ef1,self.owa(envyEFAg),sum(utilAg),self.owamax(utilAg),np.product(utilAg)]
        
        
    def setObjective(self,meth):
        """
        Set the objective function of the MIP. If meth equals :
        0 : no objective function
        1 : minmize the number of envied agents
        2 : minimize the overall envied utility
        """
        
        if meth==0:
            self.model.setObjective(0, GRB.MINIMIZE)
            self.model.update()
        elif meth==1:
            sumObjective = LinExpr()
            for i in range(self.m):
                for k in range(self.m):
                    for j in range(self.n): 
                        sumObjective.addTerms(self.u[i][j], self.z_var[i,j])
            self.model.setObjective(sumObjective, GRB.MAXIMIZE)
            self.model.update()
    def pareto2(self,l):
        """
        Applique un filtre de Pareto
        """
        remove=[]
        
        for ind1 in range(len(l)):
            for ind2 in range(len(l)):
                if ind1!=ind2:
                    dom=True
                    for i in range(len(l[ind1])):
                        dom = (l[ind1][i]>=l[ind2][i]) and dom
                    if dom:
                        #print l[ind1],"domine",l[ind2]
                        remove.append(ind2)
        """
        if len(remove)>0:
            print len(remove),"\n\n",remove,"\n\n",l
            raise ValueError
        """
            
        res=[]    
        for i in range(len(l)):
            if i not in remove:
                res.append(l[i])
                
        return res
                        
    def owa(self,s):
        """
        renvoie la valeur de l owa associée au vecteur
        """
        res=0
        s.sort()
        s.reverse() # min OWA so decreasing vectors
        for i in range(len(s)):
            res+=s[i]*self.owaw[i]
            
        return res
    
    def owamax(self,s):
        """
        renvoie la valeur de l owa associée au vecteur
        """
        res=0
        s.sort()
        #s.reverse() # min OWA so decreasing vectors
        for i in range(len(s)):
            res+=s[i]*self.owaw[i]
            
        return res
    
    def setVariables(self):
        # declaration des variables zij=0 si on l'agent i ne prend pas l'objet j et zij=1 si l'agent i le prend
        self.z_var = {}
        for i in range(self.m): # pour tout agent i
            for j in range(self.n): # pour tout objet j
                self.z_var[i,j] = self.model.addVar(vtype=GRB.BINARY,name='z_'+str(i)+'_'+str(j))
            
        self.model.update()
        
    def setConstraints(self):        

        # contrainte vérifiant si l'utilite de chaque agent de la solution
        # est superieure a celle donnée en entrée
        for i in range(self.m): # pour tout agent
            utili = LinExpr()
            for j in range(self.n): # pour tout objet
                utili.addTerms(self.u[i][j], self.z_var[i,j])
        
            self.model.addConstr(utili, GRB.GREATER_EQUAL,self.sol[i])
        
        # contrainte un objet appartient à un seul agent
        for j in range(self.n): # pour tout objet
            obji = LinExpr()
            for i in range(self.m): # pour tout agent
                obji.addTerms(1, self.z_var[i,j])
            self.model.addConstr(obji, GRB.EQUAL, 1)
        
        self.model.update()
        
    def minEmpty(self,l):
        """
        Redefinit min : renvoie min(l) si l est non vide, renvoie 0 sinon 
        """
        if len(l)==0:
            return 0
        else:
            return min(l)
        
if __name__=="__main__": 
    #np.random.seed(12222222)
    
    forced=False                    # vaut True si on veut rentrer les utilités à la main
    samePref=False
    
    nbAgs = 4
    nbObjs = 10
    
    nbIterations = 1
    
    # on utilise les poids (1,0,....,0) pour revenir a MinMax
    owa_weights = np.zeros(nbAgs)
    owa_weights[0]=1
    
    owa_weights2 = np.zeros(nbAgs)
    for indOw in range(nbAgs):
        owa_weights2[indOw]=1/(1.0*(2**(indOw+1)))
        
    owa_weights3 = np.random.rand(nbAgs)
    #print(owa_weights3)
    
    owa_weights4 = np.ones((nbAgs))
    
    normalU = 1000            # la valeur a laquelle somme l'utilité de tout agent
        
    for nbIte in range(nbIterations):
        #print "\n"
        u = np.zeros((nbAgs,nbObjs))
        if not forced:
            if samePref:
                prefId = r.RandIntVec(nbObjs, normalU, Distribution=r.RandFloats(nbObjs))
                for ag in range(nbAgs):
                    u[ag] = prefId
            else:
                for ag in range(nbAgs):
                    u[ag] = r.RandIntVec(nbObjs, normalU, Distribution=r.RandFloats(nbObjs))
        
        #print u
        syst = System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights4,sol=[1000,0,0,0],meth=1)
        #sys2 = System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights2)
        # max OWA util
        #sysm = sysmax.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights)
        #sysm2 = sysmax.System(nbObj=nbObjs,nbAg=nbAgs,U=u,owaw=owa_weights2)
        print(u)
        deb=time.time()
        print(syst.showRes())
        print(syst.isPareto(),"\n\n\n")
        #print(time.time()-deb)
