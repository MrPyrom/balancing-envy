# Balancing Envy

You can find here the code that we ran for the tests of the experimental results of the paper "Minimizing and Balancing Envy Among Agents Using Ordered Weighted Average" presented at ADT 2021. Note that in order to run the two "main.py" (mainRandom.py and mainSpliddit.py) you need to have gurobipy installed and have the spliddit instances from the Spliddit site. We warmly thank Ariel Procaccia, Nisarg Shah and Jonathan Goldman for sharing their data with us.
